<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>

	<footer role="contentinfo" class="footer">

		<div class="footer-nav">
			<?php 			
				$ga = new GlobalArea('Footer Nav');
				$ga->setBlockLimit(1);
				$ga->display($c);			
			?>
		</div>

		<div class="footer-info">

			<a aria-hidden="true" href="#top" class="icon-top"><span class="visually-hidden">Top</span></a>

			&copy; <?php echo date('Y')?> <a href="<?php echo DIR_REL?>/"><?php echo SITE?></a>,
			<?php echo t('PALS Certification Institute, All Rights Reserved.')?><br />
			<?php 
			$u = new User();
			if ($u->isRegistered()) { ?>
				<?php  
				if (Config::get("ENABLE_USER_PROFILES")) {
					$userName = '<a href="' . $this->url('/profile') . '">' . $u->getUserName() . '</a>';
				} else {
					$userName = $u->getUserName();
				}
				?>
				<span class="sign-in"><?php echo t('Currently logged in as <b>%s</b>.', $userName)?> <a href="<?php echo $this->url('/login', 'logout')?>"><?php echo t('Sign Out.')?></a></span>
			<?php  } else { ?>
				<span class="sign-in"><a href="<?php echo $this->url('/login')?>"><?php echo t('Log in to your account.')?></a></span>
			<?php  } ?>

		</div><!-- end .footer-info -->	
            
	</footer>

</div><!-- end .page -->

<?php  Loader::element('footer_required'); ?>

<script src="<?=$this->getThemePath()?>/js/functions.js"></script>

<script src="<?=$this->getThemePath()?>/js/respond.min.js"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</body>

</html>