<?php   defined('C5_EXECUTE') or die("Access Denied.");
$page = Page::getCurrentPage();
$menu_highlight = $page->getAttribute('menu_highlight');
?>
<!doctype html>
<html dir="ltr" lang="en-US">

<head>
	<meta charset="UTF-8">

	<?php   Loader::element('header_required');
	$secure = $c->getCollectionAttributeValue('jbx_force_ssl');
	 $uinfo = new User();
	?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php if ($secure){ ?>
	<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic' rel='stylesheet' type='text/css'>
<?php } else { ?>
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,400italic' rel='stylesheet' type='text/css'>
<?php } ?>
	<link rel="stylesheet" href="<?=$this->getThemePath()?>/css/styles.css">

	<script src="<?=$this->getThemePath()?>/js/jquery.fitvids.js"></script>

</head>

<body>

  <div class="page">

	<header role="banner" class="header">

		<div class="header-area">

			<div class="top-module">
			<ul class="nav-top">
				<li class="nav-top-item"><a href="<?php
			if($uinfo->IsLoggedIn()){
			echo BASE_URL.DIR_REL."/member";
			} else {
			echo BASE_URL.DIR_REL."/login";
			}
			?>">My Account</a></li>
			<li class="nav-top-item"><a href="<?php echo BASE_URL.DIR_REL; ?>/course-registration" class="register">Register</a></li>
			<li class="nav-top-item"><a href="<?php echo BASE_URL.DIR_REL; ?>/group-discounts">Group Orders</a></li>
			<li class="nav-top-item"><a href="<?php echo BASE_URL.DIR_REL; ?>/contact" class="phone-contact">1-800-448-2078</a></li>
			<li class="social-icons">
				<a aria-hidden="true" href="https://www.facebook.com/ACLScertification" target="_blank" class="icon-facebook"><span class="visually-hidden">Facebook</span></a>
				<a aria-hidden="true" href="https://twitter.com/ACLSinstitute" target="_blank" class="icon-twitter"><span class="visually-hidden">Twitter</span></a>
				<a aria-hidden="true" href="https://plus.google.com/+Aclscertificationonline" target="_blank" class="icon-google-plus"><span class="visually-hidden">Google+</span></a>
				<a aria-hidden="true" href="http://www.linkedin.com/in/aclscertification" target="_blank" class="icon-linkedin"><span class="visually-hidden">Linkedin</span></a>
				<a aria-hidden="true" href="https://www.pinterest.com/aclsinstitute/" target="_blank" class="icon-pinterest"><span class="visually-hidden">Pinterest</span></a>
				<a aria-hidden="true" href="http://instagram.com/aclscertification" target="_blank" class="icon-instagram-logo"><span class="visually-hidden">Instagram</span></a>
				<a aria-hidden="true" href="http://www.youtube.com/aclsinstitute" target="_blank" class="icon-youtube-2"><span class="visually-hidden">YouTube</span></a>
				<a aria-hidden="true" href="http://www.aclscertification.com/tools/blocks/sb_blog_list/rss?cID=437" target="_blank" class="icon-feed"><span class="visually-hidden">RSS</span></a>
		</li>
		</ul>
		</div>

			<div class="main-logo">
				<a href="<?php echo DIR_REL?>/"><img src="<?=$this->getThemePath()?>/img/pals-logo.jpg" alt="PALS Certification Institute"></a>
			</div>

			<div class="header-logos">

				<div class="trust-symbols">
				<a href="//privacy-policy.truste.com/click-to-verify/www.aclscertification.com" title="Privacy Policy by TRUSTe" class="trust-symbol" target="_blank"><img alt="Privacy Policy by TRUSTe" src="<?=$this->getThemePath()?>/img/h.png" /></a>
				<a href="https://trustsealinfo.verisign.com/splash?form_file=fdf/splash.fdf&dn=www.aclscertification.com&lang=en" title="Norton Secured Powered By VeriSign" target="_blank"><img alt="Privacy Policy by TRUSTe" src="<?=$this->getThemePath()?>/img/norton-verisign.png" /></a>
				</div>

				<form class="search-form" method="get" action="<?php echo View::url('/search-results'); ?>">
					<input type="text" name="query" class="search-input" placeholder="Search" />
					<input type="submit" value="Go" class="btn-red btn-small" />
				</form>
			</div>

			<div class="menu-container">

			<a href="#menu" class="menu-link">Main Menu</a>

			<nav role="navigation" id="menu" class="menu">

				<?php
				$ga = new GlobalArea('Header Nav');
				$ga->setBlockLimit(1);
				$ga->display();
				?>
			</nav>

			</div><!-- end .menu-container -->

		</div><!-- end .header-area -->

				<?php
				$a = new Area('Header Image');
				$a->setBlockLimit(1);
				$a->display($c);
				?>

	</header>