<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>

	<?php
	$a = new Area('Top Span');
	$a->display($c);
	?>

<div role="main" class="main">

	<?php
	$a = new Area('Learning Center');
	$a->display($c);
	?>

	<?php
	$a = new Area('Main');
	$a->display($c);
	?>
</div><!-- end .main -->

<aside role="complementary" class="secondary">
	<?php
	$as = new Area('Sidebar');
	$as->display($c);
	?>
</aside><!-- end sidebar -->

<?php  $this->inc('elements/footer.php'); ?>
