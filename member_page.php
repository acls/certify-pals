<?php  
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>
<div id="carea" class="ccm-ui">
<div id="repeatFullCont">
<div id="topFullCont">
				<?php  
				$a = new Area('Top Span');
				$a->display($c);
				?>
				
<div id="memberinsideright" class="margin-left:15px;margin-bottom:15px">

				<?php  
				$a = new GlobalArea('Member Sidebar');
				$a->display($c);
				?>

			</div> 
<div class="breadcrumb-navigation">
<?php
$a = new GlobalArea('Breadcrumb Nav');
$a->display($c);
?>
</div>

<?php  
				$a = new Area('Main');
				$a->display($c);
				?>
			</div>
			<div id="bottomFullCont"></div>
		</div>
	</div>
	<!-- end sidebar -->
<?php  $this->inc('elements/footer.php'); ?>

