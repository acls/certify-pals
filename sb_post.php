<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');

$html = Loader::helper('html');
$this->addHeaderItem($html->css('page_types/sb_post.css', 'simpleblog'));
?>

<div role="main" class="main simple-box">

        <?php
          	$a = new Area('Main');
          	$a->display($c);
        ?>

        <?php
      	$a = new Area('Blog Post More');
      	$a->display($c);
        ?>

        <?php
          	$a = new Area('Blog Post Footer');
          	$a->display($c);
        ?>

</div><!-- end .main -->

<aside role="complementary" class="secondary">

  <?php
  $as = new Area('Sidebar');
  $as->display($c);
  ?>

</aside>

<?php $this->inc('elements/footer.php'); ?>