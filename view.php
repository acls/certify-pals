<?php  
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>

	<div role="main" class="main">
		<?php print $innerContent; ?>
	</div>

<?php  $this->inc('elements/footer.php'); ?>